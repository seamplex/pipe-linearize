SetFactory("OpenCASCADE");

l = 2*(b-a);


Cylinder(1) = {-l/2, 0, 0,  +l, 0, 0,  b};
Cylinder(2) = {-l/2, 0, 0,  +l, 0, 0,  a};
BooleanDifference(3) = {Volume{1};Delete;}{Volume{2};Delete;};

Physical Volume("pipe") = {3};
s() = Boundary{ Volume{3}; };
Physical Surface("symmetry") = {s(1), s(2)};
Physical Surface("pressure") = {s(3)};

Mesh.CharacteristicLengthMax = (b-a)/n;

Mesh.Algorithm = 6;
Mesh.ElementOrder = order;
