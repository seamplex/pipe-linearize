#!/bin/sh
for i in `cat deps-plot`; do
  if [ -z "`which $i`" ]; then
    echo "error: $i not installed (or not found)"
    exit 1
  fi
done

for i in analytical.ppl convergence-1.dat convergence-2.dat; do
  if [ ! -e $i ]; then
    echo "error: no results found, please read README fist"
    exit 1
  fi
done

n=`grep PARAMETRIC pipe.fin | awk '{print $6}'`

# convert wasora's ^ to pyxplot's **, and := to =
sed 's/\^/**/g' problem.was | sed 's/:=/=/' > problem.ppl
pyxplot plot.ppl

for i in `seq 1 ${n}`; do
  gmsh -v 0 pipe-draw-mesh-1-${i}.geo
  convert -trim pipe-mesh-1-${i}.png pipe-mesh-1-${i}.png;
  convert -trim pipe-mesh-zoom-1-${i}.png pipe-mesh-zoom-1-${i}.png;  
  montage -mode concatenate -geometry +50 -tile 2x1 pipe-mesh-1-${i}.png pipe-mesh-zoom-1-${i}.png pipe-mesh-${i}.png
done 
