# analytical stress linearization of a infinite pipe
# of internal radius a, external radius b and internal pressure p

# problem definition (in a separate file shared by the FEM solution)
INCLUDE problem.was

# principal stresses along the radial coordinate (may be y or z)
sigma1(r) := sigmatheta(0,0,r)
sigma2(r) := sigmal(0,0,r)
sigma3(r) := sigmar(0,0,r)

# computation of main membrane stresses
M_1 = 1/(b-a)*integral(sigma1(r), r, a, b)
M_2 = 1/(b-a)*integral(sigma2(r), r, a, b)
M_3 = 1/(b-a)*integral(sigma3(r), r, a, b)

# von mises and tresca membrane stresses
M_tresca = max(abs(M_1-M_2), abs(M_2-M_3), abs(M_3-M_1))
M_vonmises = sqrt(((M_1-M_2)^2 + (M_2-M_3)^2 + (M_3-M_1)^2)/2)

# computation of membrane plus bending stresses
MB_1 = M_1 + 6/(b-a)^2*integral(sigma1(r)*((a+b)/2-r), r, a, b)
MB_2 = M_2 + 6/(b-a)^2*integral(sigma2(r)*((a+b)/2-r), r, a, b)
MB_3 = M_3 + 6/(b-a)^2*integral(sigma3(r)*((a+b)/2-r), r, a, b)

# von mises and tresca
MB_tresca = max(abs(MB_1-MB_2), abs(MB_2-MB_3), abs(MB_3-MB_1))
MB_vonmises = sqrt(((MB_1-MB_2)^2 + (MB_2-MB_3)^2 + (MB_3-MB_1)^2)/2)

# print results
PRINT "\# analytical results in [ MPa ]"
PRINT "M_tresca    = " M_tresca
PRINT "M_vonmises  = " M_vonmises
PRINT "M_1         = " M_1
PRINT "M_2         = " M_2
PRINT "M_3         = " M_3
PRINT
PRINT "MB_tresca   = " MB_tresca
PRINT "MB_vonmises = " MB_vonmises
PRINT "MB_1        = " MB_1
PRINT "MB_2        = " MB_2
PRINT "MB_3        = " MB_3

