#!/bin/sh
for i in `cat deps-run`; do
  if [ -z "`which $i`" ]; then
    echo "error: $i not installed (or not found)"
    exit 1
  fi
done

./versions.sh > versions.txt
./cpu.sh > cpu.txt

# get problem parameters for maxima from definition in wasora
grep " = " problem.was | awk '{print $1" : "$3";"}' | grep -v l > problem.max

# compute analytical results with maxima
maxima -b analytical.max > analytical.txt

# compute theoretical results with wasora
wasora analytical.was | tee analytical.ppl

# run fem parametric cases
fino pipe.fin 1 | tee convergence-1.dat
fino pipe.fin 2 | tee convergence-2.dat

