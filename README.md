# Fino case: ASME linearization of an infinite pipe

This repository contains the input files to analyze the stress linearization of an infinite pressurized pipe.
The associated report is <https://seamplex.com/fino/doc/pipe-linearized/>.

# Execution

Required packages:

 * Fino
 * Gmsh
 * wasora
 * Maxima

```
./run.sh
```
 
# Plotting

Required packages:

 * Pyxplot
 * ImageMagick
 
```
./plot.sh
```


